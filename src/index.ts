import { Box } from 'ogl';

// export { default as plant } from './createPlantGeometry';
export { default as grid } from './geometry/grid';

const cube = (
  gl: WebGL2RenderingContext,
  props: { width: number; depth: number; height: number },
) => {
  console.log(props);
  return new Box(gl, props);
};

export { cube };
